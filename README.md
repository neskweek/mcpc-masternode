# **Installation :**

1. Install docker (google is your friend)   

2. put those files in a directory   

3. enter the directory   

4. Construct the image : 
```
   docker build -t mcpc:latest \
                --build-arg USERNAME=youruser \
                --build-arg UID=${UID} \
                --build-arg GID=${GID} \
                --build-arg PRIVKEY=93HaYBVUCYjEMeeH1Y4sBGLALQZE1Yc1K64xiqgX37tGBDQL8Xg \
                .
                
   mkdir -p /var/cache/MCPCoin
   chown $UID:root -R /var/cache/MCPCoin
```
   
5. Launch the container :

```
docker run  --name MCPCoin --rm \
            -e DISPLAY=$DISPLAY \
            -e NODEIP=11.22.33.44 \                              #optional
            -v /tmp/.X11-unix:/tmp/.X11-unix  \ 
            -v /usr/share/X11/xkb:/usr/share/X11/xkb \
            -v /var/cache/MCPCoin:/var/cache/MCPCoin \
            -p 49451:49451 \
            -p 49452:49452  \
             mcpc:latest
```
