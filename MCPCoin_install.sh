#!/bin/bash

TMP_FOLDER=$(mktemp -d)
CONFIG_FILE='MCPCoin.conf'
CONFIGFOLDER='/var/cache/MCPCoin'
COIN_PATH='/usr/local/bin/'
COIN_DAEMON='MCPCoind'
COIN_CLI='MCPCoin-cli'
COIN_TGZ='https://github.com/MobilePayCoin/MobilePayCoin/releases/download/v1.2.1.0/MCPCoin-v1.2.1-linux.tar.gz'
COIN_ZIP='MCPCoin-v1.2.1-linux.tar.gz'
COIN_NAME='MCPCoin'
COIN_PORT=49451
RPC_PORT=49452


CYAN='\033[1;36m'
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

function download_node() {
  echo -e "Prepare to download ${GREEN}$COIN_NAME{$NC} binaries"
  cd $TMP_FOLDER
  wget $COIN_TGZ
  tar xvzf $COIN_ZIP -C $COIN_PATH >/dev/null 2>&1
  compile_error
  chmod +x $COIN_PATH$COIN_DAEMON $COIN_PATH$COIN_CLI
  cd - >/dev/null 2>&1
  rm -r $TMP_FOLDER >/dev/null 2>&1
  clear
}


function create_config() {
  echo -e "Creation of the configuration"
  mkdir -p $CONFIGFOLDER >/dev/null 2>&1
  RPCUSER=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w10 | head -n1)
  RPCPASSWORD=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w22 | head -n1)
  cat << EOF > $CONFIGFOLDER/$CONFIG_FILE
rpcuser=$RPCUSER
rpcpassword=$RPCPASSWORD
rpcallowip=0.0.0.0
rpcport=$RPC_PORT
listen=1
server=1
daemon=1
port=$COIN_PORT
EOF
}

function create_key() {
  COINKEY=$PRIVKEY
  if [[ -z "$COINKEY" ]]; then
        echo -e "${GREEN}Using the user defined key"
        $COIN_PATH$COIN_DAEMON -daemon -daemon -conf=$CONFIGFOLDER/$CONFIG_FILE -datadir=$CONFIGFOLDER
        sleep 30
        if [ -z "$(ps axo cmd:100 | grep $COIN_DAEMON)" ]; then
            echo -e "${RED}$COIN_NAME server couldn not start. Check /var/log/syslog for errors.{$NC}"
            exit 1
        fi
        COINKEY=$($COIN_PATH$COIN_CLI masternode genkey)
        if [ "$?" -gt "0" ]; then
            echo -e "${RED}Generating a new ${GREEN}PrivateKey${NC}"
            sleep 30
            COINKEY=$($COIN_PATH$COIN_CLI masternode genkey)
        fi
        $COIN_PATH$COIN_CLI stop
    fi
clear
}

function update_config() {
  sed -i 's/daemon=1/daemon=0/' $CONFIGFOLDER/$CONFIG_FILE
  cat << EOF >> $CONFIGFOLDER/$CONFIG_FILE
#bind=$NODEIP
masternode=1
staking=1
externalip=$NODEIP:$COIN_PORT
masternodeaddr=$NODEIP:$COIN_PORT
masternodeprivkey=$COINKEY
upnp=1
EOF
}


function get_ip() {
  NODEIP=$(curl ifconfig.me)
}


function compile_error() {
if [ "$?" -gt "0" ]; then
  echo -e "${RED}Failed to compile $COIN_NAME. Please investigate.${NC}"
  exit 1
fi
}


#######
#Useless in Docker
function checks() {
if [[ $(lsb_release -d) != *16.04* ]]; then
  echo -e "${RED}You are not running Ubuntu 16.04. Installation is cancelled.${NC}"
  exit 1
fi

if [[ $EUID -ne 0 ]]; then
   echo -e "${RED}$0 must be run as root.${NC}"
   exit 1
fi


if [ -n "$(pidof $COIN_DAEMON)" ] || [ -e "$COIN_DAEMOM" ] ; then
  echo -e "${RED}$COIN_NAME is already installed.${NC}"
  exit 1
fi
}
###########

function important_information() {
 echo
 echo -e "================================================================================================================================"
 echo -e "$COIN_NAME Masternode is up and running listening on port ${RED}$COIN_PORT${NC}."
 echo -e "Configuration file is: ${RED}$CONFIGFOLDER/$CONFIG_FILE${NC}"
 echo -e "Start: ${RED}systemctl start $COIN_NAME.service${NC}"
 echo -e "Stop: ${RED}systemctl stop $COIN_NAME.service${NC}"
 echo -e "VPS_IP:PORT ${GREEN}$NODEIP:$COIN_PORT${NC} - ${TESTIP}"
 echo -e "MASTERNODE PRIVATEKEY is: ${GREEN}$COINKEY${NC}"
 echo -e "Please check ${GREEN}$COIN_NAME${NC} is running with the following command: ${GREEN}systemctl status $COIN_NAME.service${NC}"
 echo -e "For check masternode status just use: ${GREEN} $COIN_CLI masternode status"
 echo -e "${CYAN} Thanks for supporting us, if you need help do not hesitate to ask us."
 echo -e "${NC} Never give your logs for teamviewer or other, be careful !!"
 echo -e "================================================================================================================================"
}

function setup_node() {
  get_ip
  create_config
  create_key
  update_config
  important_information
}


##### Main #####
clear
checks
download_node
setup_node
