FROM ubuntu:16.04

ENV TERM dumb
ARG USERNAME
ARG UID
ARG GID
ARG PRIVKEY


RUN echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin-ubuntu-bitcoin-bionic.list 
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys D46F45428842CE5E

RUN apt-get    update 
RUN apt-get -y upgrade
RUN apt-get -y install  ca-certificates \
                        wget\
                        curl \
                        build-essential \
                        libssl-dev \
                        libboost-all-dev \
                        libqrencode-dev \
                        libminiupnpc-dev \
                        libdb4.8-dev \
                        libdb4.8++-dev \
                        git \
                        lsb-core \
                        make \
                        software-properties-common \
                        libtool \
                        autoconf \
                        sudo \
                        automake \
                        bsdmainutils \
                        libminiupnpc-dev \
                        libgmp3-dev \
                        pkg-config \
                        libevent-dev \
                        libzmq5 \
                        libx11-xcb1 \
                        libfontconfig1
        
RUN mkdir /opt/mcpcoin
RUN cd /opt/mcpcoin        
ADD MCPCoin_install.sh . 
RUN chmod +x MCPCoin_install.sh
RUN bash MCPCoin_install.sh
#RUN apt-get purge build-essential libssl-dev libboost-all-dev libqrencode-dev libminiupnpc-dev libdb4.8-dev libdb4.8++-dev git
RUN rm -r /var/lib/apt/lists/*



# Replace 1000 with your user / group id
RUN mkdir -p /home/${USERNAME} && \
    echo "${USERNAME}:x:${UID}:${GID}:MCPCoin Masternode,,,:/home/${USERNAME}:/bin/bash" >> /etc/passwd && \
    echo "${USERNAME}:x:${UID}:" >> /etc/group && \
    echo "${USERNAME} ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/${USERNAME} && \
    chmod 0440 /etc/sudoers.d/${USERNAME} && \
    chown ${UID}:${GID} -R /home/${USERNAME} && \
    chown ${UID}:root -R /var/cache/MCPCoin

USER ${USERNAME}
ENV HOME /home/${USERNAME}

#ENTRYPOINT ["/bin/sleep"]
#CMD ["/bin/sleep","infinity"]
CMD /usr/local/bin/MCPCoin-qt -daemon -conf=/var/cache/MCPCoin/MCPCoin.conf -datadir=/var/cache/MCPCoin -masternode=1 -staking=1 -server=1 -upnp
